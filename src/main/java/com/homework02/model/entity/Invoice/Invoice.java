package com.homework02.model.entity.Invoice;

import com.homework02.model.entity.Customer.Customer;
import com.homework02.model.entity.Product.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    private Integer invoiceId;
    private Date date;
    private Customer customer;
    private List<Product> products;
}
