package com.homework02.controller.Product;

import com.homework02.model.entity.Product.Product;
import com.homework02.model.request.Product.ProductRequest;
import com.homework02.model.entity.ResponseHandler;
import com.homework02.service.Product.ProductService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@Tag(name="Product API")
@RequestMapping("api/v1/product")
public class ProductController {
    ResponseHandler<List<Product>> responseHandler;

    private final ProductService productService;
    public ProductController(ProductService productService){
        this.productService = productService;
    }


    @GetMapping("/")
    public ResponseEntity<ResponseHandler<List<Product>>> getAllProduct(){
        responseHandler = new ResponseHandler<>(productService.getAllProduct(), "Fetch all products successfully", true, new Date());
        return ResponseEntity.ok().body(responseHandler);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseHandler<List<Product>>> getProductByID(@PathVariable Integer id){
        responseHandler = new ResponseHandler<>(productService.getProductByID(id), "Fetch product successfully", true, new Date());
        if (responseHandler.getPayload().isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(responseHandler);
    }

    @PostMapping("/add")
    public ResponseEntity<ResponseHandler<List<Product>>> addProduct(@RequestBody ProductRequest productRequest){
        if (productRequest.getName().equals("string") || productRequest.getPrice() == 0){
            responseHandler = new ResponseHandler<>(null, "Default value not allowed", false, new Date());
            return ResponseEntity.badRequest().body(responseHandler);
        }
        responseHandler = new ResponseHandler<>(productService.addProduct(productRequest), "Add successfully", true, new Date());
        return ResponseEntity.ok().body(responseHandler);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseHandler<?>> updateProductById(@PathVariable Integer id, @RequestBody ProductRequest productRequest){
        if (productRequest.getName().equals("string") || productRequest.getPrice() == 0){
            responseHandler = new ResponseHandler<>(null, "Default value not allowed", false, new Date());
            return ResponseEntity.badRequest().body(responseHandler);
        }
        responseHandler = new ResponseHandler<>(productService.updateProductById(id, productRequest), "Update successfully", true, new Date());
        if (responseHandler.getPayload().isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(responseHandler);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseHandler<List<Product>>> deleteProductById(@PathVariable Integer id){
        responseHandler = new ResponseHandler<>(productService.deleteProductById(id), "Deleted successfully", true, new Date());
        if (responseHandler.getPayload().isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(responseHandler);
    }


}
