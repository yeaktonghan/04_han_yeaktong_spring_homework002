package com.homework02.controller.Invoice;

import com.homework02.model.entity.Invoice.Invoice;
import com.homework02.model.entity.ResponseHandler;
import com.homework02.model.request.Invoice.InvoiceRequest;
import com.homework02.service.Invoice.InvoiceService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@Tag(name = "Invoice API")
@RequestMapping("api/v1/invoice")
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    ResponseHandler<List<Invoice>> responseHandler;


    @GetMapping("/")
    public ResponseEntity<ResponseHandler<List<Invoice>>> getAllInvoice(){
        responseHandler = new ResponseHandler<>(invoiceService.getAllInvoice(), "Successfully get all invoice", true, new Date());
        return ResponseEntity.ok().body(responseHandler);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseHandler<List<Invoice>>> getInvoiceByID(@PathVariable Integer id){
        responseHandler = new ResponseHandler<>(invoiceService.getInvoiceByID(id), "Get invoice successfully", true, new Date());
        if (responseHandler.getPayload().isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(responseHandler);
    }

    @PostMapping("/add")
    public ResponseEntity<ResponseHandler<List<Invoice>>> addInvoice(@RequestBody InvoiceRequest invoiceRequest){
        if (invoiceRequest.getCustomer() == 0|| invoiceRequest.getProducts().get(0) == 0){
            responseHandler = new ResponseHandler<>(null, "Default value not allowed", false, new Date());
            return ResponseEntity.badRequest().body(responseHandler);
        }
        responseHandler = new ResponseHandler<>(invoiceService.addInvoice(invoiceRequest), "Get invoice successfully", true, new Date());
        return ResponseEntity.ok().body(responseHandler);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseHandler<List<Invoice>>> getInvoiceByID(@PathVariable Integer id, @RequestBody InvoiceRequest invoiceRequest){
        if (invoiceRequest.getCustomer() == 0|| invoiceRequest.getProducts().get(0) == 0){
            responseHandler = new ResponseHandler<>(null, "Default value not allowed", false, new Date());
            return ResponseEntity.badRequest().body(responseHandler);
        }
        responseHandler = new ResponseHandler<>(invoiceService.updateInvoice(id,invoiceRequest), "Get invoice successfully", true, new Date());
        if (responseHandler.getPayload().isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(responseHandler);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseHandler<List<Invoice>>> deleteInvoiceById(@PathVariable Integer id){
        responseHandler = new ResponseHandler<>(invoiceService.deleteProductById(id), "Deleted successfully", true, new Date());
        if (responseHandler.getPayload().isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(responseHandler);
    }
}
