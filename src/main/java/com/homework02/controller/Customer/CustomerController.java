package com.homework02.controller.Customer;
import com.homework02.model.entity.Customer.Customer;
import com.homework02.model.entity.ResponseHandler;
import com.homework02.model.request.Customer.CustomerRequest;
import com.homework02.service.Customer.CustomerService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
@Tag(name="Customer API")
@RestController
@RequestMapping("api/v1/customer")
public class CustomerController {
    ResponseHandler<List<Customer>> responseHandler;

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }


    @GetMapping("/")
    public ResponseEntity<ResponseHandler<List<Customer>>> getAllCustomer(){
        responseHandler = new ResponseHandler<>(customerService.getAllCustomer(), "Fetch All Customer successfully", true, new Date());
        return ResponseEntity.ok().body(responseHandler);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseHandler<List<Customer>>> getCustomerById(@PathVariable Integer id){
        responseHandler = new ResponseHandler<>(customerService.getCustomerById(id), "Fetch Customer successfully", true, new Date());
        if (responseHandler.getPayload().isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(responseHandler);
    }

    @PostMapping("/add")
    public ResponseEntity<ResponseHandler<List<Customer>>> addCustomer(@RequestBody CustomerRequest customerRequest){
        if (customerRequest.getName().equals("string") || customerRequest.getAddress().equals("string") || customerRequest.getPhone().equals("string")){
            responseHandler = new ResponseHandler<>(null, "Default value not allowed", false, new Date());
            return ResponseEntity.badRequest().body(responseHandler);
        }
        responseHandler = new ResponseHandler<>(customerService.addCustomer(customerRequest), "Customer added successfully", true, new Date());
        return ResponseEntity.ok().body(responseHandler);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseHandler<List<Customer>>> updateCustomer(@PathVariable Integer id, @RequestBody CustomerRequest customerRequest){
        if (customerRequest.getName().equals("string") || customerRequest.getAddress().equals("string") || customerRequest.getPhone().equals("string")){
            responseHandler = new ResponseHandler<>(null, "Default value not allowed", false, new Date());
            return ResponseEntity.badRequest().body(responseHandler);
        }
        responseHandler = new ResponseHandler<>(customerService.updateCustomer(id, customerRequest), "Customer updated successfully", true, new Date());
        if (responseHandler.getPayload().isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(responseHandler);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseHandler<List<Customer>>> deleteCustomer(@PathVariable Integer id){
        responseHandler = new ResponseHandler<>(customerService.deleteCustomer(id), "Customer deleted successfully", true, new Date());
        if (responseHandler.getPayload().isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(responseHandler);
    }
}
