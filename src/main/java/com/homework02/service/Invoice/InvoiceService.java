package com.homework02.service.Invoice;

import com.homework02.model.entity.Invoice.Invoice;
import com.homework02.model.request.Invoice.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();

    List<Invoice> getInvoiceByID(Integer id);

    List<Invoice> addInvoice(InvoiceRequest invoiceRequest);

    List<Invoice> updateInvoice(Integer id, InvoiceRequest invoiceRequest);

    List<Invoice> deleteProductById(Integer id);
}
