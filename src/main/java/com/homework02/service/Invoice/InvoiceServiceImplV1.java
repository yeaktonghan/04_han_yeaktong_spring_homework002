package com.homework02.service.Invoice;


import com.homework02.model.entity.Invoice.Invoice;
import com.homework02.model.request.Invoice.InvoiceRequest;
import com.homework02.repository.Invoice.InvoiceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImplV1 implements InvoiceService{
    private final InvoiceRepository repository;

    public InvoiceServiceImplV1(InvoiceRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<Invoice> getAllInvoice() {
        return repository.getAllInvoice();
    }

    @Override
    public List<Invoice> getInvoiceByID(Integer id) {
        return repository.getInvoiceByID(id);
    }

    @Override
    public List<Invoice> addInvoice(InvoiceRequest invoiceRequest) {
        Integer invoiceId = repository.addInvoice(invoiceRequest);
        for (Integer product : invoiceRequest.getProducts()){
            repository.insertIntoInvoiceDetail(invoiceId, product);
        }
        return repository.getInsertedInvoice(invoiceId);
    }

    @Override
    public List<Invoice> updateInvoice(Integer id, InvoiceRequest invoiceRequest) {
        repository.deleteInvoiceDetailByInvoiceId(id);
        for (Integer product : invoiceRequest.getProducts()){
            repository.insertIntoInvoiceDetail(id, product);
        }
        return repository.getUpdatedInvoice(id, invoiceRequest);
    }

    @Override
    public List<Invoice> deleteProductById(Integer id) {
        return repository.deleteInvoice(id);
    }
}
