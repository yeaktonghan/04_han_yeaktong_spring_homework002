package com.homework02.service.Customer;

import com.homework02.model.entity.Customer.Customer;
import com.homework02.model.request.Customer.CustomerRequest;
import com.homework02.repository.Customer.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImplV1 implements CustomerService{
    private CustomerRepository customerRepository;
    public CustomerServiceImplV1(CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }
    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public List<Customer> getCustomerById(Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public List<Customer> addCustomer(CustomerRequest customerRequest) {
        return customerRepository.addCustomer(customerRequest);
    }

    @Override
    public List<Customer> updateCustomer(Integer id, CustomerRequest customerRequest) {
        return customerRepository.updateCustomer(id, customerRequest);
    }

    @Override
    public List<Customer> deleteCustomer(Integer id) {
        return customerRepository.deleteCustomer(id);
    }
}
