package com.homework02.service.Customer;

import com.homework02.model.entity.Customer.Customer;
import com.homework02.model.request.Customer.CustomerRequest;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomer();

    List<Customer>  getCustomerById(Integer id);

    List<Customer> addCustomer(CustomerRequest customerRequest);

    List<Customer> updateCustomer(Integer id, CustomerRequest customerRequest);

    List<Customer> deleteCustomer(Integer id);
}
