package com.homework02.service.Product;

import com.homework02.model.entity.Product.Product;
import com.homework02.model.request.Product.ProductRequest;
import com.homework02.repository.Product.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImplV1 implements ProductService{

    public ProductServiceImplV1(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    private final ProductRepository productRepository;


    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public List<Product> getProductByID(Integer id) {
        return productRepository.getProductByID(id);
    }

    @Override
    public List<Product> addProduct(ProductRequest productRequest) {
        return productRepository.addProduct(productRequest);
    }

    @Override
    public List<Product> updateProductById(Integer id, ProductRequest productRequest) {
        return productRepository.updateProductById(id, productRequest);
    }

    @Override
    public List<Product> deleteProductById(Integer id) {
        return productRepository.deleteProductById(id);
    }
}
