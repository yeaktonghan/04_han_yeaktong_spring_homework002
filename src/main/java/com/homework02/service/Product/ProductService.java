package com.homework02.service.Product;

import com.homework02.model.entity.Product.Product;
import com.homework02.model.request.Product.ProductRequest;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();
    List<Product> getProductByID(Integer id);

    List<Product> addProduct(ProductRequest productRequest);

    List<Product> updateProductById(Integer id, ProductRequest productRequest);

    List<Product> deleteProductById(Integer id);
}
