package com.homework02.repository.Invoice;

import com.homework02.model.entity.Invoice.Invoice;
import com.homework02.model.request.Invoice.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {
    @Select("""
            SELECT * FROM invoice;
            """)
    @Result(property = "invoiceId", column = "invoice_id")
    @Result(property = "customer", column = "customer_id",
            one = @One(select = "com.homework02.repository.Customer.CustomerRepository.getCustomerById"))
    @Result(property = "products", column = "invoice_id",
            many = @Many(select = "com.homework02.repository.InvoiceDetail.InvoiceDetailRepository.getProductByInvoiceId"))
    List<Invoice> getAllInvoice();


    @Select("""
            SELECT * FROM invoice
            WHERE invoice_id = #{id};
            """)
    @Result(property = "invoiceId", column = "invoice_id")
    @Result(property = "customer", column = "customer_id",
            one = @One(select = "com.homework02.repository.Customer.CustomerRepository.getCustomerById"))
    @Result(property = "products", column = "invoice_id",
            many = @Many(select = "com.homework02.repository.InvoiceDetail.InvoiceDetailRepository.getProductByInvoiceId"))
    List<Invoice> getInvoiceByID(Integer id);


    @Select("""
            INSERT INTO invoice
            VALUES (DEFAULT, now(), #{invoiceReq.customer})
            RETURNING invoice_id;
            """)
    Integer addInvoice(@Param("invoiceReq") InvoiceRequest invoiceRequest);

    @Insert("""
            INSERT INTO invoice_detail
            VALUES (DEFAULT, #{invoiceId}, #{productId})
            """)
    void insertIntoInvoiceDetail(Integer invoiceId, Integer productId);


    @Select("""
            SELECT * FROM invoice
            WHERE invoice_id = #{invoiceId};
            """)
    @Result(property = "invoiceId", column = "invoice_id")
    @Result(property = "customer", column = "customer_id",
            one = @One(select = "com.homework02.repository.Customer.CustomerRepository.getCustomerById"))
    @Result(property = "products", column = "invoice_id",
            many = @Many(select = "com.homework02.repository.InvoiceDetail.InvoiceDetailRepository.getProductByInvoiceId"))
    List<Invoice> getInsertedInvoice(Integer invoiceId);


    @Delete("""
            DELETE
            FROM invoice_detail
            WHERE invoice_id = #{id};
            """)
    void deleteInvoiceDetailByInvoiceId(Integer id);

    @Select("""
            SELECT * FROM invoice
            WHERE invoice_id = #{invoiceId};
            """)
    @Result(property = "invoiceId", column = "invoice_id")
    @Result(property = "customer", column = "customer_id",
            one = @One(select = "com.homework02.repository.Customer.CustomerRepository.getCustomerById"))
    @Result(property = "products", column = "invoice_id",
            many = @Many(select = "com.homework02.repository.InvoiceDetail.InvoiceDetailRepository.getProductByInvoiceId"))
    List<Invoice> getUpdatedInvoice(Integer invoiceId, InvoiceRequest invoiceRequest);


    @Delete("""
            DELETE
            FROM invoice
            WHERE invoice_id = #{id};
            RETURNING *
            """)
    List<Invoice> deleteInvoice(Integer id);
}
