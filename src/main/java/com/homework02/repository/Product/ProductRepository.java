package com.homework02.repository.Product;

import com.homework02.model.entity.Product.Product;
import com.homework02.model.request.Product.ProductRequest;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("""
            SELECT * FROM product
            ORDER BY product_id;
            """)
    @Result(property = "productId", column = "product_id")
    List<Product> getAllProduct();


    @Select("""
            SELECT *
            FROM product
            WHERE product_id = #{id};
            """)
    @Result(property = "productId", column = "product_id")
    List<Product> getProductByID(Integer id);


    @Select("""
            INSERT INTO product
            VALUES (DEFAULT, #{product.name}, #{product.price})
            RETURNING *;
            """)
    @Result(property = "productId", column = "product_id")
    List<Product> addProduct(@Param("product") ProductRequest productRequest);


    @Select("""
            UPDATE product
            SET name = #{product.name}, price = #{product.price}
            WHERE product_id = #{id}
            RETURNING *;
            """)
    @Result(property = "productId", column = "product_id")
    List<Product> updateProductById(Integer id,@Param("product") ProductRequest productRequest);


    @Select("""
            DELETE FROM product
            WHERE product_id = #{id}
            RETURNING *;
            """)
    @Result(property = "productId", column = "product_id")
    List<Product> deleteProductById(Integer id);
}
