package com.homework02.repository.InvoiceDetail;

import com.homework02.model.entity.Product.Product;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface InvoiceDetailRepository {
    @Select("""
            SELECT p.product_id, p.name, p.price FROM invoice_detail AS i
                INNER JOIN product p ON i.product_id = p.product_id
            WHERE invoice_id = #{id};
            """)
    @Result(property = "productId", column = "product_id")
    List<Product> getProductByInvoiceId(Integer id);
}
