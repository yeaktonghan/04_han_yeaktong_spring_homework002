package com.homework02.repository.Customer;

import com.homework02.model.entity.Customer.Customer;
import com.homework02.model.request.Customer.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {

    @Select("""
            SELECT * FROM customer
            ORDER BY customer_id;
            """)
    @Result(property = "customerId", column = "customer_id")
    List<Customer> getAllCustomer();

    @Select("""
            SELECT * FROM customer
            WHERE customer_id = #{id};
            """)
    @Result(property = "customerId", column = "customer_id")
    List<Customer> getCustomerById(Integer id);

    @Select("""
            INSERT INTO customer
            VALUES (DEFAULT, #{customer.name}, #{customer.address}, #{customer.phone})
            RETURNING *;
            """)
    @Result(property = "customerId", column = "customer_id")
    List<Customer> addCustomer(@Param("customer") CustomerRequest customerRequest);


    @Select("""
            UPDATE customer 
            SET name = #{customer.name},address = #{customer.address},phone = #{customer.phone}
            WHERE customer_id = #{id}
            RETURNING *;
            """)
    @Result(property = "customerId", column = "customer_id")
    List<Customer> updateCustomer(Integer id, @Param("customer") CustomerRequest customerRequest);


    @Select("""
            DELETE FROM customer
            WHERE customer_id = #{id}
            RETURNING *;
            """)
    @Result(property = "customerId", column = "customer_id")
    List<Customer> deleteCustomer(Integer id);
}
